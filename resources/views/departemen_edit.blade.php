@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('departemen') }}">Data Departemen</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('departemen/create') }}">Tambah Data Departemen</a>
                            </li>
                          </ul>
                    </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('departemen/'.$collection[0]->id)}}" method="POST">
                        @csrf
                        @foreach($collection as $item)
                        <div class="modal-body">
                            <div class="form-group">
                                <label>  Nama Departemen</label>
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="id" id="id" value="{{$item->id}}">
                                <input type="text" name="nama_departemen" id="nama_departemen" class="form-control" value="{{$item->nama_departemen}}">
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success">Edit</button>
                                @endforeach
                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
