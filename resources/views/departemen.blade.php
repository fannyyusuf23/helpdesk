@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('departemen') }}">Data Departemen</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('departemen/create') }}">Tambah Data Departemen</a>
                            </li>
                          </ul>
                    </div>

                    <div class="card-body">
                 <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped " id="datatables">
                    <thead>
                    <tr>
                        <th>Nama Departemen</th>
                        <th>Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $item)
                        <tr>
                            <td>{{$item->nama_departemen}}</td>
                            <td>
                                <a href="{{url('departemen/'.$item->id.'/edit')}}">
                                    <button type="button" class="btn btn-outline-primary">Edit</button>
                                </a>
                            <form class="" action="{{url('departemen/'.$item->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                    <input type="submit" class="btn btn-outline-danger" value="Delete">
                            </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
