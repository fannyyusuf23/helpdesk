@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('pelayanan/'.$collection[0]->id)}}" method="POST">
                        @csrf
                        @foreach($collection as $item)
                        <div class="modal-body">
                            <div class="form-group">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="id" id="id" value="{{$item->id}}">
                                <input type="hidden" name="task_status" id="task_status" class="form-control" value="On Proses">
                                <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <label class="input-group-text" for="task_priority">Priority</label>
                                  </div>
                                  <select class="custom-select" id="task_priority" name="task_priority">
                                    <option selected>{{$item->task_priority}}</option>
                                    <option value="Reguler" name="task_priority">Reguler</option>
                                    <option value="Priority" name="task_priority">Priority</option>
                                    <option value="Urgent" name="task_priority">Urgent</option>
                                  </select>
                                </div>
                                </div>
                                 <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="nama_departemen">Jenis Request </label>
                                <div class=" col-sm-8">
                                <select name="jenis_request_id" id="jenis_request_id" class="form-control">

                                    <option selected>{{$item->jenis_request_id}}</option>
                                    @foreach( $collections as $item )
                                    <option value="{{$item->id}}">{{$item->jenis_request}}</option>
                                    @endforeach
                                </select>

                                </div>
                            </div>
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success">Update</button>
                                @endforeach
                        </form>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
