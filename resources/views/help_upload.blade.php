@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Help Desk</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('help')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="task_file">Upload File</label>
                                    <input type="file" name="file" id="file" multiple ></select>
                                    <input type="hidden" name="task_id" id="task_id" value="{{}}">
                                </div>
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success">kirim</button>

                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
