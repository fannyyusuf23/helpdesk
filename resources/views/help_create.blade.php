@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('help') }}">List Helpdesk</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('help/create') }}">Tambah Data Helpdesk</a>
                        </li>
                      </ul>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('help')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="nama_departemen">Departemen </label>
                                <div class=" col-sm-10">
                                <select name="departement_id" id="departement_id" class="form-control">
                                    @foreach( $collection as $item )
                                    <option value="{{$item->id}}">{{$item->nama_departemen}}</option>
                                    @endforeach
                                </select>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label>  Nama </label>
                                <input type="text" name="costumer_name" id="costumer_name" class="form-control" value="{{ Auth::user()->name }}">
                                <?php
                                    $no = 0;
                                    $no++ ;
                                    $tgltiket = date('ymd');
                                    $tiket_number=$tgltiket.$no;
                                    $tiket_number++;
                                    $tglmulai = date('Y-m-d H:i:s');
                                    $tglselesai = date('Y-m-d H:i:s');

                                ?>
                                <input type="hidden" name="ticket_number" id="ticket_number" class="form-control" value="{{$tiket_number++}}">
                                <input type="hidden" name="task_start_date" id="task_start_date" class="form-control" value="{{$tglmulai}}">
                                <input type="hidden" name="task_end_date" id="task_end_date" class="form-control" value="{{$tglselesai}}">
                                <input type="hidden" name="task_officer" id="task_officer" class="form-control" value="-">
                                <input type="hidden" name="task_status" id="task_status" class="form-control" value="Input">
                                <input type="hidden" name="delete_by" id="delete_by" class="form-control" value="-">
                            <input type="hidden" name="jenis_request_id" id="jenis_request_id" class="form-control" value='{{$no}}'>
                            </div>
                            <div class="form-group">
                                <label> Judul </label>
                                <input type="text" name="task_title" id="task_title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label> Permasalahan </label>
                                <textarea name="task_desk" id="task_desk" class="form-control" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="task_priority"> Prioritas </label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority"  value="Reguler" checked> Reguler
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority" value="Priority" checked> Priority
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority" value="Urgent" checked> Urgent
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="file">Pilih File</label>
                                <input type="file" name="file[]" id="file" multiple>
                            </div>

                                <button type="submit" name="save" id="save" class="btn btn-success" href="{{ url('help') }}">
                                    Kirim
                                </button>
                        </form>

                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
