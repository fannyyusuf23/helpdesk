@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('pelayanan') }}">Data task</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('pelayanan/create') }}">Tambah Data Jenis Request</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('petugas/create') }}">Tambah Data Jenis Request</a>
                              </li>
                          </ul>
                    </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('petugas')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label>  Nama Pegawai</label>
                                <input type="text" name="nama_pegawai" id="nama_pegawai" class="form-control">
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="id_departement">Departement</label>
                                <div class=" col-sm-8">
                                <select name="id_departement" id="id_departement" class="form-control">
                                    <option selected>Pilih....</option>
                                    @foreach( $collectionn as $item )
                                    <option value="{{$item->id}}">{{$item->nama_departemen}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label" for="id_departement">ID Users</label>
                                <div class=" col-sm-8">
                                <select name="id_user" id="id_user" class="form-control">
                                    <option selected>Pilih....</option>
                                    @foreach( $collection as $item )
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                                </div>
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success" href="{{ url('pelayanan') }}">Submit</button>
                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
