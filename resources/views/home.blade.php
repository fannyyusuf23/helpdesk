@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Help Desk</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('home')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label" for="nama_departemen">Departemen </label>
                                <div class=" col-sm-10">
                                <select name="nama_departemen" id="nama_departemen" class="form-control">
                                    @foreach($collection as $item)
                                    <option value="{{$item->id}}">{{$item->nama_departemen}}</option>
                                    @endforeach
                                </select>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label>  Nama </label>
                                <input type="text" name="costumer_name" id="costumer_name" class="form-control">
                            </div>
                            <div class="form-group">
                                <label> Judul </label>
                                <input type="text" name="task_title" id="task_title" class="form-control">
                            </div>
                            <div class="form-group">
                                <label> Permasalahan </label>
                                <textarea name="task_desk" id="task_desk" class="form-control" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="task_priority"> Prioritas </label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority"  value="Reguler" checked> Reguler
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority" value="Priority" checked> Priority
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="task_priority" id="task_priority" value="Urgent" checked> Urgent
                                    </label>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label for="task_file">Pilih File</label>
                                <input type="file" name="task_fie" id="task_file"></select>
                            </div>-->

                                <button type="submit" name="save" id="save" class="btn btn-success">kirim</button>

                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
