@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('pelayanan') }}">Data Task</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('pelayanan/create') }}">Tambah Data Jenis Request</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="{{ url('petugas/create') }}">Tambah Data Petugas</a>
                              </li>
                          </ul>
                    </div>

                    <div class="card-body">
                 <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped " id="datatables">
                    <thead>
                    <tr>
                        <th>Nama Costumer</th>
                        <th>Priority</th>
                        <th>Departement</th>
                        <th>Jenis Request</th>
                        <th>Deskripsi</th>
                        <th>Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $item)
                        <tr>
                            <td>{{$item->costumer_name}}</td>
                            <td>{{$item->task_priority}}</td>
                            <td>{{$item->departement_id}}</td>
                            <td>{{$item->jenis_request_id}}</td>
                            <td>{{$item->task_desk}}</td>
                            <td>
                                <a href="{{url('pelayanan/'.$item->id.'/edit')}}">
                                    <button type="button" class="btn btn-outline-primary">Update</button>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
