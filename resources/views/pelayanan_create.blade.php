@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs card-header-tabs">
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('pelayanan') }}">Data task</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ url('pelayanan/create') }}">Tambah Data Jenis Request</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ url('petugas/create') }}">Tambah Data Petugas</a>
                              </li>
                          </ul>
                    </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('pelayanan')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label>  Nama Jenis Request</label>
                                <input type="text" name="jenis_request" id="jenis_request" class="form-control">
                            </div>
                                <button type="submit" name="save" id="save" class="btn btn-success" href="{{ url('pelayanan') }}">Submit</button>
                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
