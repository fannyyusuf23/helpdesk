@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Help Desk</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{url('departemen')}}" method="POST">
                        @csrf
                        @foreach($collection as $item)
                        <div class="modal-body">
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Departemen </label>
                                <div class=" col-sm-10">
                                <select name="nama_departemen" id="nama_departemen" class="form-control">
                                    <option value=""> {{$item->nama_departemen}}</option>
                                </select>
                                 </div>
                            </div>
                            <div class="form-group">
                                <label>  Nama </label>
                                <input type="text" name="kode_siswa" id="kode_siswa" class="form-control">
                            </div>
                            <div class="form-group">
                                <label> Judul </label>
                                <input type="text" name="nama_siswa" id="nama_siswa" class="form-control">
                            </div>
                            <div class="form-group">
                                <label> Permasalahan </label>
                                <textarea name="nama_siswa" id="nama_siswa" class="form-control" rows="6"></textarea>
                            </div>
                            <div class="form-group">
                                <label> Prioritas </label>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="nama_siswa" id="nama_siswa"  value="Biasa" checked> Biasa
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="nama_siswa" id="nama_siswa" value="Biasa" checked> Sangat Penting
                                    </label>
                                </div>
                            </div>

                                <button type="submit" name="save" id="save" class="btn btn-success">kirim</button>
                                @endforeach
                        </form>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
