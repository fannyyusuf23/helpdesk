@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs card-header-tabs">
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('help') }}">List Helpdesk</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="{{ url('help/create') }}">Tambah Data Helpdesk</a>
                        </li>
                      </ul>
                    <div class="card-body">
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Data Success</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                 <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped " id="datatables">
                    <thead>
                    <tr>
                        <th>Nama Costumer</th>
                        <th>Priority</th>
                        <th>Request</th>
                        <th>Status</th>

                    </tr>
                    </thead>
                    <tbody>
                        @foreach($collection as $item)
                        <tr>
                            <td>{{$item->costumer_name}}</td>
                            <td>{{$item->task_priority}}</td>
                            <td>{{$item->departement_id}}</td>
                            <td>{{$item->task_status}}</td>
                        </tr>
                        @endforeach
                    </tbody>

                </div>
            </div>
        </div>

    </div>
</div>
@endsection
