<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::get('/home','HomeController@index');
Auth::routes();
Route::resource('help', 'HelpController');
Route::resource('departemen', 'DeptController');
Route::resource('pelayanan', 'PITController');
Route::resource('petugas', 'PetController');

