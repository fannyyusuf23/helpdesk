<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\Console\Helper\Table;

class Table1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_request', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('jenis_request');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');
        });
        Schema::create('tbl_pegawai', function (Blueprint $table) {
            $table->Increments('id_pegawai');
            $table->string('nama_pegawai');
            $table->integer('id_departement');
            $table->integer('id_user');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');
        });
        Schema::create('departemen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_departemen');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');
        });

        Schema::create('task', function (Blueprint $table) {
            $table->increments('id');
            $table->string('costumer_name');
            $table->string('task_title');
            $table->string('task_desk');
            $table->string('ticket_number');
            $table->dateTime('task_start_date');
            $table->dateTime('task_end_date');
            $table->string('task_officer');
            $table->string('task_priority');
            $table->string('task_status');
            $table->integer('departement_id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');
        });

        Schema::create('task_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->string('task_comment');
            $table->dateTime('comment_date');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');

        });


        Schema::create('task_file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file');
            $table->dateTime('task_id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('delete_by');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
