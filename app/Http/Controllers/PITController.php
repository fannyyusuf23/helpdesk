<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class PITController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection=DB::table('task')->get();
        $collections=DB::table('jenis_request')->get();
        return view('pelayanan',["collection"=>$collection],["collections"=>$collections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pelayanan_create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['jenis_request']=$request->input('jenis_request');
        DB::table('jenis_request')->insert($data);
        return redirect('pelayanan');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $collection = DB:: table('task')->where('id',$id)->get();
        $collections=DB::table('jenis_request')->get();
        $collectionn=DB::table('tbl_pegawai')->get();
        return view('pelayanan_edit', ["collection"=>$collection] , ["collections"=>$collections],["collectionn"=>$collectionn]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('task')->where('id',$request->id)->update([
            'task_priority'=>$request->task_priority,
            'task_status'=>$request->task_status,
            'jenis_request_id'=>$request->jenis_request_id
        ]);

        // alihkan halaman ke halaman pegawai
        return redirect('pelayanan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
