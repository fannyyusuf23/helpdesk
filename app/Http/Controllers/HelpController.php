<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\File;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Session;

class HelpController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $collection=DB::table('task')->join('tbl_pegawai','task.departement_id','=','tbl_pegawai.id_departement')
        ->select('*')->get();
        return view('help',["collection"=>$collection]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $collection=DB::table('departemen')->get();
        return view('help_create',["collection"=>$collection]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        // generate a pin based on 2 * 7 digits + a random character
        $pin = mt_rand(1000000, 9999999)
         . mt_rand(1000000, 9999999)
         . $characters[rand(0, strlen($characters) - 1)];

        // shuffle the result
        $string = str_shuffle($pin);
        $data['costumer_name']=$request->input('costumer_name');
        $data['task_title']=$request->input('task_title');
        $data['task_desk']=$request->input('task_desk');
        $data['task_priority']=$request->input('task_priority');
        $data['departement_id']=$request->input('departement_id');
        $data['jenis_request_id']=$request->input('jenis_request_id');
        $data['ticket_number']=$string;
        $data['task_start_date']=$request->input('task_start_date');
        $data['task_end_date']=$request->input('task_end_date');
        $data['delete_by']=$request->input('delete_by');
        $data['task_officer']=$request->input('task_officer');
        $data['task_status']=$request->input('task_status');

        DB::table('task')->insert($data);
        return redirect('help');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
