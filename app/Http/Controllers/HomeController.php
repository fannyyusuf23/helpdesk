<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $collection=DB::table('departemen')->get();
        return view('home',["collection"=>$collection]);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['costumer_name']=$request->input('costumer_name');
        $data['task_title']=$request->input('task_title');
        $data['task_desk']=$request->input('task_desk');
        $data['task_priority']=$request->input('task_priority');
        $data['ticket_number']=$request->input('ticket_number');
        $data['departement_id']=$request->input('departement_id');
        $data['task_start_date']=$request->input('task_start_date');
        $data['task_end_date']=$request->input('task_end_date');
        $data['delete_by']=$request->input('delete_by');

        DB::table('task')->insert($data);
        return redirect('home');
    }
}
